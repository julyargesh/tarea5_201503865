/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PeticionesCliente;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS
 */
public class PeticionesClienteTest {
    
    public PeticionesClienteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Pedir_Uber method, of class PeticionesCliente.
     */
    @Test
    public void testPedir_Uber() {
        System.out.println("Pedir_Uber");
        String id = "C1";
        PeticionesCliente instance = new PeticionesCliente();
        String expResult = "z11";
        String result = instance.Pedir_Uber(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        if(!result.equals(expResult))
            fail("The test case is a prototype.");
    }
    
}
