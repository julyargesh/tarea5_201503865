
package peticionespiloto;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the peticionespiloto package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AlmacenarDatos_QNAME = new QName("http://PeticionesPiloto/", "AlmacenarDatos");
    private final static QName _AlmacenarDatosResponse_QNAME = new QName("http://PeticionesPiloto/", "AlmacenarDatosResponse");
    private final static QName _ObtenerPiloto_QNAME = new QName("http://PeticionesPiloto/", "ObtenerPiloto");
    private final static QName _ObtenerPilotoResponse_QNAME = new QName("http://PeticionesPiloto/", "ObtenerPilotoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: peticionespiloto
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AlmacenarDatosResponse }
     * 
     */
    public AlmacenarDatosResponse createAlmacenarDatosResponse() {
        return new AlmacenarDatosResponse();
    }

    /**
     * Create an instance of {@link ObtenerPiloto }
     * 
     */
    public ObtenerPiloto createObtenerPiloto() {
        return new ObtenerPiloto();
    }

    /**
     * Create an instance of {@link AlmacenarDatos }
     * 
     */
    public AlmacenarDatos createAlmacenarDatos() {
        return new AlmacenarDatos();
    }

    /**
     * Create an instance of {@link ObtenerPilotoResponse }
     * 
     */
    public ObtenerPilotoResponse createObtenerPilotoResponse() {
        return new ObtenerPilotoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlmacenarDatos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://PeticionesPiloto/", name = "AlmacenarDatos")
    public JAXBElement<AlmacenarDatos> createAlmacenarDatos(AlmacenarDatos value) {
        return new JAXBElement<AlmacenarDatos>(_AlmacenarDatos_QNAME, AlmacenarDatos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlmacenarDatosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://PeticionesPiloto/", name = "AlmacenarDatosResponse")
    public JAXBElement<AlmacenarDatosResponse> createAlmacenarDatosResponse(AlmacenarDatosResponse value) {
        return new JAXBElement<AlmacenarDatosResponse>(_AlmacenarDatosResponse_QNAME, AlmacenarDatosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerPiloto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://PeticionesPiloto/", name = "ObtenerPiloto")
    public JAXBElement<ObtenerPiloto> createObtenerPiloto(ObtenerPiloto value) {
        return new JAXBElement<ObtenerPiloto>(_ObtenerPiloto_QNAME, ObtenerPiloto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerPilotoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://PeticionesPiloto/", name = "ObtenerPilotoResponse")
    public JAXBElement<ObtenerPilotoResponse> createObtenerPilotoResponse(ObtenerPilotoResponse value) {
        return new JAXBElement<ObtenerPilotoResponse>(_ObtenerPilotoResponse_QNAME, ObtenerPilotoResponse.class, null, value);
    }

}
