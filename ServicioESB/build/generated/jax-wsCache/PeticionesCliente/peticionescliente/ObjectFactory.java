
package peticionescliente;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the peticionescliente package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PedirUber_QNAME = new QName("http://PeticionesCliente/", "PedirUber");
    private final static QName _AlmacenarDatosResponse_QNAME = new QName("http://PeticionesCliente/", "AlmacenarDatosResponse");
    private final static QName _AlmacenarDatos_QNAME = new QName("http://PeticionesCliente/", "AlmacenarDatos");
    private final static QName _PedirUberResponse_QNAME = new QName("http://PeticionesCliente/", "PedirUberResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: peticionescliente
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PedirUber }
     * 
     */
    public PedirUber createPedirUber() {
        return new PedirUber();
    }

    /**
     * Create an instance of {@link AlmacenarDatosResponse }
     * 
     */
    public AlmacenarDatosResponse createAlmacenarDatosResponse() {
        return new AlmacenarDatosResponse();
    }

    /**
     * Create an instance of {@link AlmacenarDatos }
     * 
     */
    public AlmacenarDatos createAlmacenarDatos() {
        return new AlmacenarDatos();
    }

    /**
     * Create an instance of {@link PedirUberResponse }
     * 
     */
    public PedirUberResponse createPedirUberResponse() {
        return new PedirUberResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PedirUber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://PeticionesCliente/", name = "PedirUber")
    public JAXBElement<PedirUber> createPedirUber(PedirUber value) {
        return new JAXBElement<PedirUber>(_PedirUber_QNAME, PedirUber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlmacenarDatosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://PeticionesCliente/", name = "AlmacenarDatosResponse")
    public JAXBElement<AlmacenarDatosResponse> createAlmacenarDatosResponse(AlmacenarDatosResponse value) {
        return new JAXBElement<AlmacenarDatosResponse>(_AlmacenarDatosResponse_QNAME, AlmacenarDatosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlmacenarDatos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://PeticionesCliente/", name = "AlmacenarDatos")
    public JAXBElement<AlmacenarDatos> createAlmacenarDatos(AlmacenarDatos value) {
        return new JAXBElement<AlmacenarDatos>(_AlmacenarDatos_QNAME, AlmacenarDatos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PedirUberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://PeticionesCliente/", name = "PedirUberResponse")
    public JAXBElement<PedirUberResponse> createPedirUberResponse(PedirUberResponse value) {
        return new JAXBElement<PedirUberResponse>(_PedirUberResponse_QNAME, PedirUberResponse.class, null, value);
    }

}
