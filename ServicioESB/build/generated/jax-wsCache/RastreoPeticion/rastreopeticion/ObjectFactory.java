
package rastreopeticion;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the rastreopeticion package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AlmacenarDatosResponse_QNAME = new QName("http://RastreoPeticion/", "AlmacenarDatosResponse");
    private final static QName _UbicarAuto_QNAME = new QName("http://RastreoPeticion/", "UbicarAuto");
    private final static QName _UbicarAutoResponse_QNAME = new QName("http://RastreoPeticion/", "UbicarAutoResponse");
    private final static QName _AlmacenarDatos_QNAME = new QName("http://RastreoPeticion/", "AlmacenarDatos");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: rastreopeticion
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AlmacenarDatosResponse }
     * 
     */
    public AlmacenarDatosResponse createAlmacenarDatosResponse() {
        return new AlmacenarDatosResponse();
    }

    /**
     * Create an instance of {@link UbicarAuto }
     * 
     */
    public UbicarAuto createUbicarAuto() {
        return new UbicarAuto();
    }

    /**
     * Create an instance of {@link UbicarAutoResponse }
     * 
     */
    public UbicarAutoResponse createUbicarAutoResponse() {
        return new UbicarAutoResponse();
    }

    /**
     * Create an instance of {@link AlmacenarDatos }
     * 
     */
    public AlmacenarDatos createAlmacenarDatos() {
        return new AlmacenarDatos();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlmacenarDatosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://RastreoPeticion/", name = "AlmacenarDatosResponse")
    public JAXBElement<AlmacenarDatosResponse> createAlmacenarDatosResponse(AlmacenarDatosResponse value) {
        return new JAXBElement<AlmacenarDatosResponse>(_AlmacenarDatosResponse_QNAME, AlmacenarDatosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UbicarAuto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://RastreoPeticion/", name = "UbicarAuto")
    public JAXBElement<UbicarAuto> createUbicarAuto(UbicarAuto value) {
        return new JAXBElement<UbicarAuto>(_UbicarAuto_QNAME, UbicarAuto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UbicarAutoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://RastreoPeticion/", name = "UbicarAutoResponse")
    public JAXBElement<UbicarAutoResponse> createUbicarAutoResponse(UbicarAutoResponse value) {
        return new JAXBElement<UbicarAutoResponse>(_UbicarAutoResponse_QNAME, UbicarAutoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlmacenarDatos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://RastreoPeticion/", name = "AlmacenarDatos")
    public JAXBElement<AlmacenarDatos> createAlmacenarDatos(AlmacenarDatos value) {
        return new JAXBElement<AlmacenarDatos>(_AlmacenarDatos_QNAME, AlmacenarDatos.class, null, value);
    }

}
