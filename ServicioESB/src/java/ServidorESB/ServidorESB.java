/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorESB;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import peticionescliente.*;
import peticionespiloto.*;
import rastreopeticion.*;

/**
 *
 * @author ASUS
 */
@WebService(serviceName = "ServidorESB")
public class ServidorESB {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "FuncionESB")
    public String FuncionESB(@WebParam(name = "id") String idCliente) {
        PeticionesCliente_Service ServicioCliente = new PeticionesCliente_Service();
        PeticionesCliente PuertoCliente = ServicioCliente.getPeticionesClientePort();
        String ubicacion = PuertoCliente.pedirUber(idCliente);
        RastreoPeticion_Service ServicioRastreo = new RastreoPeticion_Service();
        RastreoPeticion PuertoRastreo = ServicioRastreo.getRastreoPeticionPort();
        String conductorDisponible = PuertoRastreo.ubicarAuto(ubicacion);
        PeticionesPiloto_Service ServicioPiloto = new PeticionesPiloto_Service();
        PeticionesPiloto PuertoPiloto = ServicioPiloto.getPeticionesPilotoPort();
        String informacionPiloto = PuertoPiloto.obtenerPiloto(conductorDisponible);
        return informacionPiloto;
    }
}
