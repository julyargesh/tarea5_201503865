const gulp = require('gulp');
const zip = require('gulp-zip');
const fileindex = require('gulp-fileindex');

gulp.task('fileindex', function() {
    return gulp.src('dist/*.war')
        .pipe(fileindex())
        .pipe(gulp.dest('./dist'));
});

var filesToMove = [
    'Clientes/dist/*.war',
    'Pilotos/dist/*.war',
    'Rastreo/dist/*.war',
    'ServicioESB/dist/*.war'
];

gulp.task('move', function(){
    return gulp.src(filesToMove)
        .pipe(gulp.dest('dist'));
});

gulp.task('default', gulp.series('move', 'fileindex'));